Name:           python-execnet
Version:        2.1.1
Release:        1
Summary:        Rapid multi-Python deployment
License:        MIT and GPLv2+
URL:            http://codespeak.net/execnet
Source0:        https://pypi.io/packages/source/e/execnet/execnet-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  procps-ng
%description
execnet provides carefully tested means to ad-hoc interact with Python interpreters across version,
platform and network barriers. It provides a minimal and fast API targetting the following uses:
distribute tasks to local or remote processes
write and deploy hybrid multi-process applications
write scripts to administer multiple hosts

%package   -n   python3-execnet
Summary:        Rapid multi-Python deployment
BuildRequires:  python3-devel python3-setuptools python3-setuptools_scm python3-sphinx python3-py
BuildRequires:	python3-pbr python3-pip python3-wheel python3-hatchling python3-hatch-vcs
BuildRequires:	python3-pytest python3-pytest-timeout python3-tox python3-gevent python3-eventlet
%{?python_provide:%python_provide python3-execnet}

%description -n python3-execnet
execnet provides carefully tested means to ad-hoc interact with Python interpreters across version,
platform and network barriers. It provides a minimal and fast API targetting the following uses:
distribute tasks to local or remote processes
write and deploy hybrid multi-process applications
write scripts to administer multiple hosts

%prep
%autosetup -n execnet-%{version} -p1

%build
SETUPTOOLS_SCM_PRETEND_VERSION=%{version}
%pyproject_build

%install
SETUPTOOLS_SCM_PRETEND_VERSION=%{version}
%pyproject_install
CFLAGS="${CFLAGS:-${RPM_OPT_FLAGS}}" LDFLAGS="${LDFLAGS:-${RPM_LD_FLAGS}}" \
PATH="%{buildroot}%{_bindir}:$PATH" \
PYTHONPATH="${PYTHONPATH:-%{buildroot}%{python3_sitearch}:%{buildroot}%{python3_sitelib}}" \
PYTHONDONTWRITEBYTECODE=1 \
%{?__pytest_addopts:PYTEST_ADDOPTS="${PYTEST_ADDOPTS:-} %{__pytest_addopts}"} \
PYTEST_XDIST_AUTO_NUM_WORKERS=%{_smp_build_ncpus} \
sphinx-build -b html doc html
rm -rf html/{.buildinfo,.doctrees}

%check
PYTEST_SELECT='not test_stdouterrin_setnull[gevent]'
PYTEST_SELECT+=' and not test_dont_write_bytecode'
PYTEST_SELECT+=' and not test_popen_io[gevent-sys.executable]'
PYTEST_SELECT+=' and not [gevent-socket]'
PYTEST_SELECT+=' and not [eventlet-socket]'
PYTEST_SELECT+=' and not [python2.7]'
CFLAGS="${CFLAGS:-${RPM_OPT_FLAGS}}" LDFLAGS="${LDFLAGS:-${RPM_LD_FLAGS}}" \
PATH="%{buildroot}%{_bindir}:$PATH" \
PYTHONPATH="${PYTHONPATH:-%{buildroot}%{python3_sitearch}:%{buildroot}%{python3_sitelib}}" \
PYTHONDONTWRITEBYTECODE=1 \
%{?__pytest_addopts:PYTEST_ADDOPTS="${PYTEST_ADDOPTS:-} %{__pytest_addopts}"} \
PYTEST_XDIST_AUTO_NUM_WORKERS=%{_smp_build_ncpus} \
%{_bindir}/pytest -k "$PYTEST_SELECT"

%files -n python3-execnet
%license LICENSE
%doc README.rst
%doc html
%{python3_sitelib}/execnet
%{python3_sitelib}/execnet*.dist-info/

%changelog
* Mon Jul 8 2024 lilu <lilu@kylinos.cn> - 2.1.1-1
- Upgrade package python3-execnet to version 2.1.1
- Fixed regression in 2.1.0 where the strconfig argument to load/loads is ignored.

* Mon Oct 23 2023 Dongxing Wang <dxwangk@isoftstone.com> - 2.0.2-1
- Upgrade package python3-execnet to version 2.0.2

* Wed Jul 05 2023 xu_ping <707078654@qq.com> - 1.9.0-2
- Add Buildrequires python3-py to fix module 'py' has no attribute 'test'

* Fri May 27 2022 OpenStack_SIG <openstack@openeuler.org> - 1.9.0-1
- Upgrade package python3-execnet to version 1.9.0

* Mon Aug 10 2020 lingsheng <lingsheng@huawei.com> - 1.7.1-1
- Update to 1.7.1

* Thu May 21 2020 yanan li <liyanan032@huawei.com> - 1.5.0-6
- Fix asserterror for testcase.

* Wed Mar 4 2020 zhouyihang<zhouyihang1@huawei.com> - 1.5.0-5
- Pakcage init
